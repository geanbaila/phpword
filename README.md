**Generar un reporte en word usando PHP**

*Este script fue escrito en PHP, usa Composer y MySQL*

---

## Otros módulos PHP necesarios

1. apt-get install php-zip 
2. apt-get install php-xml
3. apt-get install php-xmlrpc 
4. apt-get install php-mbstring

---

## Personaliza tu documento

1. El word archivo_base.docx es la plantilla sobre la cual se escriben las siguiente páginas, modificarlo en caso requiera